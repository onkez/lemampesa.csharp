#!/bin/sh

# @file: LemaMpesaBuild.sh
# @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

author="Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)"

echo
echo "Author: $author"
echo
echo "Usage: $0 [program_out_name]"
echo

extension="cs"
csc_compiler="cscd"
sources=$(ls -1 | grep -E "\.${extension}$" | tr "\n" " ")

#sources="source1.${extension} source2.${extension}"
#declare -a source_files=("source1.${extension}" "source2.${extension}" "source3.${extension}")
echo "Sources: $sources"
redir=$(read -r -a source_files <<< "$sources")

rv=1
compiler_output=""
app_name=""
info_website="https://www.mono-project.com/download/stable/"

if [ "$1" != "" ]; then
	
	app_name="$1"

elif [ "" != "${source_files[0]}" ]; then
	
	app_name=$(echo "${source_files[0]}" | tr -d ".${extension}")

fi

where_the_hell=$(whereis -lb $csc_compiler | grep -E "$csc_compiler\s*:\s*" | tr -d "$csc_compiler[:blank:]:[:blank:]")

if [ "" != "$where_the_hell" ]; then
	
	compiler_output=`$csc_compiler /t:exe /out:$app_name $source_files`
	rv=$?
	
else
	
	echo
	echo "You are missing '$csc_compiler' compiler."
	echo "To install it, please follow the guidelines at"
	echo "the site: $info_website"
	echo
	
fi

if [ "0" == "$rv" ]; then
	
	echo
	echo "Application Name: $app_name"
	echo
	
else
	
	echo
	echo "$compiler_output"
	echo
	
fi

exit $rv
