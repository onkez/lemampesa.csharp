
# 1. [Lema Mpesa Application](https://lema.io/)

It should be able to communicate with Mpesa infrastructure via its
exposed API.

# 2. [Mpesa API Documentation](https://developer.safaricom.co.ke/docs?ruby#app-dashboard)

# 3. [Mpesa API Access Logic Diagram](./MpesaAPIAccessDiagram.txt)

# 4. [Mpesa Transactional Architecture Diagram](./MobileMoney.pdf)

# 5. Requirements

Linux operating system. Windows has not been included yet. However, since the code is
written in C#, you can easily build it on Windows. The only task needed is port the *.sh
build script to Windows OS.

# 6. Build

To build the application, please, run the *.sh script in the source folder tree.
Instructions will be given as to how to fix any problems related to the building process.

# 7. Author

[Obed-Edom Nkezabahizi](onkezabahizi@gmail.com)
