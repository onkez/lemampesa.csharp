
/// @file: LemaMpesaTests.cs
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Mpesa.Api;

public class LemaMpesaTests
{
	public static int Main(string[] args)
    {
        int rv = 1;
        
        if(0 == args.Length)
        {
        	string name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        	Console.WriteLine("\r\nUsage: " + name + " <ResultPublicURL> [QueuePublicURL]\r\n");
        	return rv;
        }
        
        bool passed = false;
        bool bl = false;
        
        try
        {
        	LemaMpesaTests mpT = new LemaMpesaTests();
        	LemaMpesa mp = mpT.LemaMpesaObj;
        	
        	string[] urls = new string[args.Length];
        
        	for(int i=0; i<urls.Length; i++)
        	{
        		urls[i] = args[i];
        	}
        	
        	string rUrl = urls[0];
        	string qUrl = urls.Length > 1 ? urls[1] : urls[0];
        	
        	// B2C Request
        	var qUri = new Uri(qUrl);
        	var rUri = new Uri(rUrl);
        	string vw = "\r\n\r\n Queue Port: " + qUri.Port.ToString();
        		   vw += "\r\rResult Port: " + rUri.Port.ToString() + 
        		        "\r\n\r\n";
        	Console.Write(vw);
        	
        	if(!(bl = mpT.HttpListenerLite()))
        	{
        		Console.WriteLine("\r\nLemaMpesa::HttpListenerLite failed.");
        	}
        	else
        	{
        		Console.WriteLine("\r\nLemaMpesa::HttpListenerLite succeeded.");
        	}
        	
        	passed = passed && bl;
        	
        	/***** Mpesa APIs ******
        	// B2C
        	var b2cTask = mp.B2cApi(mpT.B2cPayload(qUrl, rUrl));
        	
        	// C2B
        	var c2bTask = mp.C2bApi(mpT.C2bPayload(qUrl, rUrl));
        	var c2bSimTask = mp.C2bApiSimulate(mpT.C2bSimulatePayload());
        	
        	// Account Balance
        	var acBalanceTask = mp.AccountBalanceApi(mpT.AccountBalancePayload(qUrl, rUrl));
        	
        	// Another Mpesa API Request
        	// etc.
            
            // Make HttpListener prefixes
            // See Remarks section in 
            // https://docs.microsoft.com/en-us/dotnet/api/system.net.httplistener?view=netframework-4.8
            List<string> prefixes = new List<string>();
            	
            		// B2C Prefix
            var b2cUri = new Uri(mp.B2cApiRequestUrl);
            string b2cPrefix = b2cUri.Scheme + "://" + b2cUri.Host + "/";
            Console.WriteLine("\r\nB2C Prefix: " + b2cPrefix);
            prefixes.Add(b2cPrefix);
            		// Another Prefix
            		// etc.
            
            /******/
            
            if(passed)
            {
            	rv = 0;
            }
        }
        catch(Exception e)
    	{
    		string method = "Main";
    		string errMessage = "LemaMpesaTests::" + method + "(): " + e.ToString();
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return rv;
    }
    
    public LemaMpesaTests()
    {
    	LemaMpesaObj = new LemaMpesa();
    }
    
    public LemaMpesa LemaMpesaObj { get; set; }
    
    public bool HttpListenerLite(string url=null)
    {
    	bool passed = false;
    	
    	try
    	{
    		if(String.IsNullOrEmpty(url))
    		{
    			url = "https://localhost:8080/";
    		}
    		
    		int maxMillisecondsWaitTime = 10000;
    		
    		// Launch the listener on the loopback in a thread.
    		Action<object> listenerAction = (object prefix) =>
    		{
    			string [] prefixes = new string[1];
    			prefixes[0] = prefix.ToString();
    			LemaMpesaObj.HttpListenerLite(prefixes);
    		};
    		Task taskListener = Task.Factory.StartNew(listenerAction, url);
    		
    		// Start clients other threads.
    		Task<string> getTaskClient = LemaMpesaObj.HttpsMethodHandler(url);
    		Task<string> postTaskClient = LemaMpesaObj.HttpsMethodHandler(url, "POST");
    		
    		// Wait for the client to get the response.
    		getTaskClient.Wait();
    		
    		//----- Check response to GET request -----------
    		bool getPassed = true;
    		string getResp = getTaskClient.Result.ToString();
    		
    		if(!String.IsNullOrEmpty(getResp))
    		{
    			Hashtable hGetResponse = LemaMpesaObj.SimpleJsonDeserializer(getResp);
    			
    			if(null != hGetResponse)
    			{
    				// Verify the keys
    				var hkeys = new string[hGetResponse.Keys.Count];
					hGetResponse.Keys.CopyTo(hkeys, 0);
					if(!Array.Exists(hkeys, e => e.ToString().Equals("HttpMethod")))
					{
						getPassed = false;
					}
					if(!Array.Exists(hkeys, e => e.ToString().Equals("ContentType")))
					{
						getPassed = false;
					}
					if(!Array.Exists(hkeys, e => e.ToString().Equals("IsLocal")))
					{
						getPassed = false;
					}
					if(!Array.Exists(hkeys, e => e.ToString().Equals("IsSecureConnection")))
					{
						getPassed = false;
					}
    			
    				// Verify the values
    				var hvalues = new string[hGetResponse.Values.Count];
					hGetResponse.Values.CopyTo(hvalues, 0);
					if(!Array.Exists(hvalues, e => e.ToString().Equals("GET")))
					{
						getPassed = false;
					}
					if(!Array.Exists(hvalues, e => e.ToString().Equals("application/json")))
					{
						getPassed = false;
					}
					if(!Array.Exists(hvalues, e => e.ToString().Equals("true")))
					{
						getPassed = false;
					}
					
					// Test Key:Val correspondence
					foreach(DictionaryEntry p in hGetResponse)
					{
						if(p.Key.ToString() == "HttpMethod")
						{
							if(p.Value.ToString() != "GET")
							{
								getPassed = false;
								break;
							}
						}
						
						else if(p.Key.ToString() == "ContentType")
						{
							if(p.Value.ToString() != "application/json")
							{
								getPassed = false;
								break;
							}
						}
						
						else if(p.Key.ToString() == "IsLocal")
						{
							if(p.Value.ToString() != "true")
							{
								getPassed = false;
								break;
							}
						}
						
						else if(p.Key.ToString() == "IsSecureConnection")
						{
							if(p.Value.ToString() != "true")
							{
								getPassed = false;
								break;
							}
						}
					}
    			}
    			else
    			{
    				getPassed = false;
    			}
    		}
    		else
    		{
    			getPassed = false;
    		}
    		//--------------------------------
    		
    		
    		postTaskClient.Wait();
    		
    		//----- Check response to POST request -----------
    		bool postPassed = true;
    		string postResp = postTaskClient.Result.ToString();
    		
    		if(!String.IsNullOrEmpty(postResp))
    		{
    			Hashtable hPostResponse = LemaMpesaObj.SimpleJsonDeserializer(postResp);
    			
    			if(null != hPostResponse)
    			{
    				// Verify the keys
    				var hkeys = new string[hPostResponse.Keys.Count];
					hPostResponse.Keys.CopyTo(hkeys, 0);
					if(!Array.Exists(hkeys, e => e.ToString().Equals("HttpMethod")))
					{
						postPassed = false;
					}
					if(!Array.Exists(hkeys, e => e.ToString().Equals("ContentType")))
					{
						postPassed = false;
					}
					if(!Array.Exists(hkeys, e => e.ToString().Equals("IsLocal")))
					{
						postPassed = false;
					}
					if(!Array.Exists(hkeys, e => e.ToString().Equals("IsSecureConnection")))
					{
						postPassed = false;
					}
    			
    				// Verify the values
    				var hvalues = new string[hPostResponse.Values.Count];
					hPostResponse.Values.CopyTo(hvalues, 0);
					if(!Array.Exists(hvalues, e => e.ToString().Equals("POST")))
					{
						postPassed = false;
					}
					if(!Array.Exists(hvalues, e => e.ToString().Equals("application/json")))
					{
						postPassed = false;
					}
					if(!Array.Exists(hvalues, e => e.ToString().Equals("true")))
					{
						postPassed = false;
					}
					
					// Test Key:Val correspondence
					foreach(DictionaryEntry p in hPostResponse)
					{
						if(p.Key.ToString() == "HttpMethod")
						{
							if(p.Value.ToString() != "POST")
							{
								postPassed = false;
								break;
							}
						}
						
						else if(p.Key.ToString() == "ContentType")
						{
							if(p.Value.ToString() != "application/json")
							{
								postPassed = false;
								break;
							}
						}
						
						else if(p.Key.ToString() == "IsLocal")
						{
							if(p.Value.ToString() != "true")
							{
								postPassed = false;
								break;
							}
						}
						
						else if(p.Key.ToString() == "IsSecureConnection")
						{
							if(p.Value.ToString() != "true")
							{
								postPassed = false;
								break;
							}
						}
					}
    			}
    			else
    			{
    				postPassed = false;
    			}
    		}
    		else
    		{
    			postPassed = false;
    		}
    		//--------------------------------
    		
    		// Send stop command to listener
    		bool stopPassed = true;
    		Hashtable stopCmd = new Hashtable();
    		stopCmd.Add("CMD", "STOP");
    		
    		string sBody = LemaMpesaObj.SimpleJsonSerializer(stopCmd);
    		if(!String.IsNullOrEmpty(sBody))
    		{
    			var stopCmdTaskClient = LemaMpesaObj.HttpsMethodHandler(url, "POST", sBody);
    			stopCmdTaskClient.Wait();
    		
    			// Wait for the listener to respond for a fixed interval of time
    			TimeSpan ts = TimeSpan.FromMilliseconds(maxMillisecondsWaitTime);
    			if(!taskListener.Wait(ts))
    			{
    				stopPassed = false;
    			}
    		    else
    		    {
    				// Check whether the client's request successfully reached the listener
    				// handled or not.
    				// Like the above
    			}
    		}
    		else
    		{
    			stopPassed = false;
    		}
    		
    		passed = getPassed && postPassed && stopPassed;
    	}
    	catch(Exception e)
    	{
    		string method = "HttpListenerLite";
    		string errMessage = "LemaMpesaTests::" + method + "(): " + e.ToString();
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return passed;
    }
	
	/// @brief Generates the test cases data as a hash table
    /// @return A Hashtable of data needed for test cases.
    /// See <a href="https://developer.safaricom.co.ke/test_credentials"> Test Data </a>
    //  and <a href="https://developer.safaricom.co.ke/docs?ruby#test-credentials"> Test Credentials </a>.
    public Hashtable TestCasesData()
    {
    	Hashtable data = null;
    	
    	try
    	{
    	
    		data = new Hashtable();
    	
    		data.Add("Shortcode1", "601365");
    		data.Add("InitiatorNameShortcode1", "apitest365");
    		data.Add("SecurityCredentialShortcode1", "Safaricom111!");
    		data.Add("Shortcode2", "600000");
    		data.Add("TestMSISDN", "254708374149");
    		data.Add("LipaNaMpesaOnlineShortcode", "174379");
    		data.Add(
    			"LipaNaMpesaOnlinePassKey", 
    			"bfb279f9aa9bdbcf158e97dd71a467cd2e0" + 
    			"c893059b10f78e6b72ada1ed2c919"
    		);
    		data.Add(
    			"GeneratedSecurityCredential", 
    			"ic3k7Fi7D8LqLyHnYX1qPHYL3lnLrKnidZ3kBiPNdNTI3sQssZ2FFixlzsQdMVFzp2" + 
    			"dbx/l/NjuRT6BpVxtXlyXYMvQSvLhTt15r2rmKinW7soLwxTQFdlljidG4HIog6X949Ug7RcAuLl8wshTH+" + 
    			"NSGmPQUILdLpWv5gTAybxuqnCLT0tIwJ7biVAh2CfdWNcY7H3tz14j9vVQrUjW3avZ/EBrsDktkqPAE9+6+" + 
    			"Z54M7lk5oj6JvG961T/tLHBmj5O5LskgJIuz4XbcUpXaR6KmaFirLqfGWFlsBShSVX8xT5e/yhs6YbBxwqY" + 
    			"6KUPoSYhmejrY62WxCXCagxUhbA=="
    		);
    	
    	}
    	catch(Exception e)
    	{
    		string method = "TestCasesData";
    		string errMessage = "LemaMpesaTests::" + method + "(): " + e.ToString();
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return data;
    }
    
    /// @brief Builds the payload data needed by C2B Simulate request
    /// @return A Json string that can directly be used by the C2B Simulate request.
    public string C2bSimulatePayload(
    	string amount=null,
    	string cmdId=null,
    	string msiSdn= null,
    	string billRefNumber=null,
    	string shortCode=null
    ){
    	string payload = null;
    	
    	try
    	{
    		
    		Hashtable hBody = new Hashtable();
    		Hashtable payloadData = TestCasesData();
    			
    		string s = String.IsNullOrEmpty(amount) ? "5400" : amount;
    		hBody.Add("Amount", s);
    		
    		s = String.IsNullOrEmpty(cmdId) ? "CustomerPayBillOnline" : cmdId;
    		hBody.Add("CommandID", s);
    		
    		s = String.IsNullOrEmpty(msiSdn) ? payloadData["TestMSISDN"].ToString() : msiSdn;
    		hBody.Add("MSISDN", s);
    		
    		s = String.IsNullOrEmpty(billRefNumber) ? "21456" : billRefNumber;
    		hBody.Add("BillRefNumber", s);
    		
    		s = String.IsNullOrEmpty(shortCode) ? payloadData["Shortcode2"].ToString() : shortCode;
    		hBody.Add("ShortCode", s);
    		
    		payload = LemaMpesaObj.SimpleJsonSerializer(hBody);
    		
    	}
    	catch(Exception e)
    	{
    		string method = "C2bSimulatePayload";
    		string errMessage = "LemaMpesaTests::" + method + "(): " + e.ToString();
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return payload;
    }
    
    /// @brief Builds the payload data needed by C2B request
    /// @return A Json string that can directly be used by the C2B request.
    /// NOTE: If any parameter is null, then the test data will be used instead.
    ///       'validationUrl' and 'confirmationUrl' must not be null
    public string C2bPayload(
    	string validationUrl,
    	string confirmationUrl,
    	string responseType = null,
    	string shortCode = null
    ){
    	string payload = null;
    	
    	try
    	{
    		if(String.IsNullOrEmpty(validationUrl) || String.IsNullOrEmpty(confirmationUrl))
    		{
    			return payload;
    		}
    		
    		Hashtable hBody = new Hashtable();
    		Hashtable payloadData = TestCasesData();
    			
    		hBody.Add("ConfirmationURL", confirmationUrl);
    			
    		hBody.Add("ValidationURL", validationUrl);
    		
    		string s = String.IsNullOrEmpty(shortCode) ? payloadData["Shortcode2"].ToString() : shortCode;
    		hBody.Add("ShortCode", s);
    		
    		s = String.IsNullOrEmpty(responseType) ? " " : responseType;
    		hBody.Add("ResponseType", s);
    		
    		payload = LemaMpesaObj.SimpleJsonSerializer(hBody);
    		
    	}
    	catch(Exception e)
    	{
    		string method = "B2cPayload";
    		string errMessage = "LemaMpesaTests::" + method + "(): " + e.ToString();
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return payload;
    }
	
	/// @brief Builds the payload data needed by B2C request
    /// @return A Json string that can directly be used by the B2C request.
    /// NOTE: If any parameter is null, then the test data will be used instead.
    ///       'queueTimeOutUrl' and 'resultUrl' must not be null
    public string B2cPayload(
    	string queueTimeOutUrl,
    	string resultUrl,
    	string initiatorName = null,
    	string securityCredential = null,
    	string cmdId = null,
    	string amount = null,
    	string partyA = null,
    	string partyB = null,
    	string remarks = null,
    	string occasion = null
    ){
    	string payload = null;
    	
    	try
    	{
    		if(String.IsNullOrEmpty(queueTimeOutUrl) || String.IsNullOrEmpty(resultUrl))
    		{
    			return payload;
    		}
    		
    		Hashtable hBody = new Hashtable();
    		Hashtable payloadData = TestCasesData();
    		 			
    		string s = String.IsNullOrEmpty(initiatorName) ? 
    		           payloadData["InitiatorNameShortcode1"].ToString() : 
    		           initiatorName;
    		hBody.Add("InitiatorName", s);
    			
    		s = String.IsNullOrEmpty(securityCredential) ? 
    		            payloadData["SecurityCredentialShortcode1"].ToString() : 
    		            securityCredential;
    		hBody.Add("SecurityCredential", s);
    			
    		s = String.IsNullOrEmpty(cmdId) ? "SalaryPayment" : cmdId;
    		hBody.Add("CommandId", s);
    			
    		s = String.IsNullOrEmpty(amount) ? "5000" : amount;
    		hBody.Add("Amount", s);
    			
    		s = String.IsNullOrEmpty(partyA) ? payloadData["Shortcode1"].ToString() : partyA;
    		hBody.Add("PartyA", s);
    			
    		s = String.IsNullOrEmpty(partyB) ? payloadData["Shortcode2"].ToString() : partyB;
    		hBody.Add("PartyB", s);
    			
    		s = String.IsNullOrEmpty(remarks) ? "No. Thanks!!" : remarks;
    		hBody.Add("Remarks", s);
    			
    		hBody.Add("QueueTimeOutURL", queueTimeOutUrl);
    			
    		hBody.Add("ResultURL", resultUrl);
    			
    		s = String.IsNullOrEmpty(occasion) ? "Afcon 2019 fi DZ" : occasion;
    		hBody.Add("Occasion", s);
    		
    		payload = LemaMpesaObj.SimpleJsonSerializer(hBody);
    		
    	}
    	catch(Exception e)
    	{
    		string method = "B2cPayload";
    		string errMessage = "LemaMpesaTests::" + method + "(): " + e.ToString();
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return payload;
    }
    
    /// @brief Builds the payload data needed by the Account Balance Mpesa API request.
    /// @return A Json string that can directly be used by the Account Balance Mpesa API request.
    /// NOTE: If any parameter is null, then the test data will be used instead.
    ///       'queueTimeOutUrl' and 'resultUrl' must not be null
    public string AccountBalancePayload(
    	string queueTimeOutUrl,
    	string resultUrl,
    	string initiatorName = null,
    	string securityCredential = null,
    	string cmdId = null,
    	string receiverIdType = null,
    	string accountType = null,
    	string partyB = null,
    	string remarks = null
    ){
    	string payload = null;
    	
    	try
    	{
    		if(String.IsNullOrEmpty(queueTimeOutUrl) || String.IsNullOrEmpty(resultUrl))
    		{
    			return payload;
    		}
    		
    		Hashtable hBody = new Hashtable();
    		Hashtable payloadData = TestCasesData();
    		 			
    		string s = String.IsNullOrEmpty(initiatorName) ? 
    		           payloadData["InitiatorNameShortcode1"].ToString() : 
    		           initiatorName;
    		hBody.Add("InitiatorName", s);
    			
    		s = String.IsNullOrEmpty(securityCredential) ? 
    		            payloadData["SecurityCredentialShortcode1"].ToString() : 
    		            securityCredential;
    		hBody.Add("SecurityCredential", s);
    			
    		s = String.IsNullOrEmpty(cmdId) ? "SalaryPayment" : cmdId;
    		hBody.Add("CommandId", s);
    			
    		s = String.IsNullOrEmpty(receiverIdType) ? "Shortcode" : receiverIdType;
    		hBody.Add("ReceiverIdentifierType", s);
    			
    		s = String.IsNullOrEmpty(accountType) ? "WTS LEMA Shortcode" : accountType;
    		hBody.Add("AccountType", s);
    			
    		s = String.IsNullOrEmpty(partyB) ? payloadData["Shortcode2"].ToString() : partyB;
    		hBody.Add("PartyB", s);
    			
    		s = String.IsNullOrEmpty(remarks) ? "No. Thanks!!" : remarks;
    		hBody.Add("Remarks", s);
    			
    		hBody.Add("QueueTimeOutURL", queueTimeOutUrl);
    			
    		hBody.Add("ResultURL", resultUrl);
    		
    		payload = LemaMpesaObj.SimpleJsonSerializer(hBody);
    		
    	}
    	catch(Exception e)
    	{
    		string method = "AccountBalancePayload";
    		string errMessage = "LemaMpesaTests::" + method + "(): " + e.ToString();
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return payload;
    }
}
