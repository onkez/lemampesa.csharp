
/// @file: LemaMpesa.cs
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Mpesa.Api
{
 
public class LemaMpesa
{
    
    public string OAuthApiResource 
    { get; set; } = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";
    
    public string B2cApiRequestUrl
    { get; set; } = "https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest";
    
    public string C2bApiRequestUrl
    { get; set; } = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl";
    
    public string C2bApiSimulateRequestUrl
    { get; set; } = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate";
    
    public string AccountBalanceApiRequestUrl
    { get; set; } = "https://sandbox.safaricom.co.ke/mpesa/accountbalance/v1/query";
    
    public string MyKey
    { get; set; } = "LpGsv1JwMKrAuMcj5taHLcBUyqnJCaQ5";
    
    public string MySecret
    { get; set; } = "dL65LAJ9j6LXXxy0";
    
    public string MpesaX509CertificateUrl
    { get; set; } = "https://developer.safaricom.co.ke/sites/default/files/cert/cert.cer";
    
    public string MpesaCertificateFile
    { get; set; } = "MpesaCertificate.cer";
    
    public string MpesaInitiatorPassword
    { get; set; } = "Love3.156/*-1234";
    
    public HttpListener MpesaHttpListener { get; set; }
    
    /// @brief Listens for incoming responses on given URIs
    /// @param prefixes: URIs to listen for.
    /// @param action: HTTP(S) Request handler.
    /// NOTE: To tell the listener to stop, enclose in the body a Jsonized hash
    ///       as follows {CMD|cmd|COMMAND|command: "stop|stop|stp|st|s"}
    public void HttpListenerLite(string[] prefixes, Action<HttpListenerContext> handler=null)
    {
    	
    	try
    	{
    		//Console.WriteLine("HttpListenerLite Called ... !");
    		
    		if(null == prefixes)
    		{
    			return;
    		}
    		if(0 == prefixes.Length)
    		{
    			return;
    		}
    		
    		MpesaHttpListener = new HttpListener();	
    	    HttpListenerContext ctx = null;
    	    bool keepForeverLoop = true;
    	    string jsonMimeTypeRegex = @"\A\s*application\s*/\s*json\s*\Z";
    	    string listenerCmdRegex = @"\Acmd|command\Z";
    	    string clientStopCmdRegex = @"\Astop|s|st|stp\Z";
    			
    		do
    		{
    			if(HttpListener.IsSupported)
    			{
    				foreach(string s in prefixes)
    				{
    					MpesaHttpListener.Prefixes.Add(s);
    				}
    		
    				MpesaHttpListener.Start();
    				MpesaHttpListener.AuthenticationSchemes = AuthenticationSchemes.Basic;
    		
    				ctx = MpesaHttpListener.GetContext();
    				HttpListenerRequest req = ctx.Request;
    				HttpListenerResponse response = ctx.Response;
    				
    				// Handle the request
    				
    				// Body
    				Hashtable hBody = null;
    				System.Text.Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
    				System.IO.Stream input = req.InputStream;
					StreamReader readStream = new StreamReader(input, encode);
					
					string sBody = readStream.ReadToEnd();
					input.Close();
					if(!String.IsNullOrEmpty(sBody))
					{
						hBody = SimpleJsonDeserializer(sBody);
					}
    				
    				if(null == handler) // Default behavior
    				{   					
    					// Headers
    					Hashtable headers = null;
    					foreach(DictionaryEntry p in req.Headers)
    					{
    						if(null == headers)
    						{
    							headers = new Hashtable();
    						}
    						headers.Add(p.Key, p.Value);
    					}
    					
    					Hashtable resp = new Hashtable();
    					
    					// Add headers
    					if(null != headers)
    					{
    						resp.Add("Headers", SimpleJsonSerializer(headers));
    					}
    					// Add method
    					resp.Add("HttpMethod", req.HttpMethod.ToString());
    					// Add body
    					if(null != hBody)
    					{
    						resp.Add("Body", SimpleJsonSerializer(hBody));
    					}
    					else
    					{
    						if(!String.IsNullOrEmpty(sBody))
							{	
    							resp.Add("Body", sBody);
    						}
    					}
    					// Add ContentType
    					resp.Add("ContentType", req.ContentType.ToString());
    					// Add IsLocal
    					resp.Add("IsLocal", req.IsLocal.ToString());
    					// Add IsSecureConnection
    					resp.Add("IsSecureConnection", req.IsSecureConnection.ToString());  
    					
    					string respString =  SimpleJsonSerializer(resp);
    					if(!String.IsNullOrEmpty(respString))
    					{
    						byte[] buffer = System.Text.Encoding.UTF8.GetBytes(respString);
    						
    						response.ContentType = "application/json";
    						response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");;
    						response.ContentLength64 = buffer.Length;
    						System.IO.Stream output = response.OutputStream;
    						output.Write(buffer, 0, buffer.Length);
    						
    						output.Close();
    					}					
    				}
    				else // Invoke action
    				{
    					handler.Invoke(ctx);
    				}
    				
    				/*** For debugging reasons only ***/
    				// If ContentType is Json check whether there is a Stop Command and
    				// Stop the server after authentication
    				if((new Regex(jsonMimeTypeRegex, RegexOptions.IgnoreCase)).IsMatch(req.ContentType.ToString()))
    				{
    					if(null != hBody)
    					{
    						foreach(DictionaryEntry p in hBody)
    						{
    							// Check command key
    							if((new Regex(listenerCmdRegex, RegexOptions.IgnoreCase)).IsMatch(p.Key.ToString()))
    							{
    								// Simple authentication
    								bool isPostMethod = (
    									new Regex(
    										@"\Apost\Z", RegexOptions.IgnoreCase
    									))
    									.IsMatch(req.HttpMethod.ToString());
    								if(isPostMethod && req.IsLocal && req.IsSecureConnection)
    								{
    									// Check 'Stop' command
    									if(
    										(new Regex(
    											clientStopCmdRegex, 
    											RegexOptions.IgnoreCase
    										)).IsMatch(p.Value.ToString()))
    									{
    										MpesaHttpListener.Stop();
    										keepForeverLoop = false;
    										break;
    									}
    								}
    							}
    						}
    					}
    				}
    				/****/
    			}
    			else
    			{
    				throw new NotSupportedException("HttpListener not supported by Mono for Linux");
    			}
    		}
    		while(keepForeverLoop);
    		
    	}
    	catch(Exception e)
    	{
    		string method = "HttpListenerLite";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}   	
    }
    
    /// @brief Account Balance Mpesa API transaction.
    /// @param payload: Body to be enclosed in the request.
    /// See #MpesaApiTransaction for details.
    /// For more info, see <a href="https://developer.safaricom.co.ke/docs?ruby#account-balance-api"> Account Balance API </a>.
    public Task<string> AccountBalanceApi(string payload, string key=null, string secret=null)
    {
    	return MpesaApiTransaction(payload, AccountBalanceApiRequestUrl, "POST", key, secret);	
    }
    
    /// @brief C2B API transaction.
    /// @param payload: Body to be enclosed in the request.
    /// See #MpesaApiTransaction for details.
    /// For more info, see <a href="https://developer.safaricom.co.ke/docs?ruby#c2b-api"> C2B API </a>.
    public Task<string> C2bApiSimulate(string payload, string key=null, string secret=null)
    {
    	return MpesaApiTransaction(payload, C2bApiSimulateRequestUrl, "POST", key, secret);	
    }
    
    /// @brief C2B API transaction.
    /// @param payload: Body to be enclosed in the request.
    /// See #MpesaApiTransaction for details.
    /// For more info, see <a href="https://developer.safaricom.co.ke/docs?ruby#c2b-api"> C2B API </a>.
    public Task<string> C2bApi(string payload, string key=null, string secret=null)
    {
    	return MpesaApiTransaction(payload, C2bApiRequestUrl, "POST", key, secret);	
    }
    
    /// @brief B2C Mpesa API transaction.
    /// @param payload: Body to be enclosed in the request.
    /// See #MpesaApiTransaction for details.
    /// For more info, see <a href="https://developer.safaricom.co.ke/docs?ruby#b2c-api"> B2C API </a>.
    public Task<string> B2cApi(string payload, string key=null, string secret=null)
    {
    	return MpesaApiTransaction(payload, B2cApiRequestUrl, "POST", key, secret);
    }
    
    /// @brief Mpesa transaction.
    /// @param payload: Data to be enclosed as body to the M-pesa API request.
    /// @param key: Mpesa API customer key.
    /// @param secret: Mpesa API customer secret.
    /// @return The received data.
    /// For more on cmdId see https://developer.safaricom.co.ke/docs?java#command-ids
    public Task<string> MpesaApiTransaction(
    	string payload, 
    	string requestUrl,
    	string http_method="POST",
    	string key=null, 
    	string secret=null
    ){
    	Task<string> result = null;
    	
    	try
    	{
    		//Console.WriteLine("B2C Called ... !");
    		
    		Hashtable oauth = MpesaOAuth();
    		string accessToken = GetAccessTokenFromOAuth(oauth);
    		
    		//Console.WriteLine("\r\nAccessToken: " + accessToken + "\r\n");
    		
    		Hashtable headers = new Hashtable();
    		headers.Add("Authorization", "Bearer " + accessToken);
    		
    		result = HttpsMethodHandler(requestUrl, http_method, payload, headers, key, secret);
    	}
    	catch(Exception e)
    	{
    		string method = "MpesaApiTransaction";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return result;
    }
    
    /// @brief Decodes a string into an array of bytes using ASCII charset
    /// @param str: An ASCII-encoded string.
    /// @return Array of bytes corresponding to the input string.
    /// NOTE: The sample Java code at the safaricom developers website uses the getBytes() Java method
    ///       that uses ASCII charset by default. Hence, we binarize strings using ASCII charset.
    ///       See https://developer.safaricom.co.ke/docs?java#security-credentials
    public byte[] GetBytes(string str)
    {
    	byte[] bytes = {};
    	
    	try
    	{
    	
    		if(null == str)
    		{
    			bytes = null;
    		}
    		else if("" == str)
    		{
    		}
    		else
    		{
    			bytes = System.Text.Encoding.ASCII.GetBytes(str);
    		}
    	
    	}
    	catch(Exception e)
    	{
    		string method = "GetBytes";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return bytes;
    }
    
    /// @brief Generates the Mpesa APIs security credentials
    /// @param password: According to <a href="https://developer.safaricom.co.ke/docs?ruby#security-credentials"> Security Credentials </a>, it is a <b> base64 encoded initiator password </b>.
    /// @return Mpesa APIs security credentials.
    public string MpesaSecurityCredentials(string password)
    {
    	string credentials = null;
    	
    	try
    	{
    		
    		RSACryptoServiceProvider RSA = null;
    		
    		// A X509 certificate. See https://developer.safaricom.co.ke/docs?ruby#security-credentials
    		var X509Cert_t = HttpsMethodHandler(MpesaX509CertificateUrl);
    		if(null != X509Cert_t)
    		{
    			X509Cert_t.Wait();
    			string X509Cert = X509Cert_t.Result;
    			//Console.WriteLine("\r\n" + X509Cert + "\r\n");
    			
    			File.WriteAllText(MpesaCertificateFile, X509Cert);
    			
    			X509Certificate2 cert = new X509Certificate2(MpesaCertificateFile);
    			// See https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography.x509certificates.x509certificate2?view=netframework-4.8#constructors
        		RSA = (RSACryptoServiceProvider)cert.PublicKey.Key;
        		
        		File.Delete(MpesaCertificateFile);
    	    }
    	    
    	    if(null != RSA)
    	    {
    	    	byte[] encryptedPassword = RSA.Encrypt(GetBytes(password), RSAEncryptionPadding.Pkcs1);
    	    	credentials = System.Convert.ToBase64String(encryptedPassword);  
    	    }	    
    	    
    	}
    	catch(Exception e)
    	{
    		string method = "MpesaSecurityCredentials";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return credentials;
    }
    
    /// @brief Format hashtable into simple Json string.
    /// @param h: Hashtable of Key:Value elements.
    /// @return A Json version of the input hash table.
    public string SimpleJsonSerializer(Hashtable h)
    {
    	string json = null;
    	
    	try
    	{
    		string[] strings = new string[h.Keys.Count];
    		int count = 0;
    		foreach(DictionaryEntry pair in h)
    		{   			  			
    			strings[count] = "\"" + pair.Key.ToString() + "\":\"" + pair.Value.ToString() + "\""; 
    			count += 1;
    		}
    		json = "{" + string.Join(",", strings) + "}";
    	}
    	catch(Exception e)
    	{
    		string method = "SimpleJsonSerializer";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return json;
    }
    
    /// @brief Converts a simple key:value Json string into a C# Hashtable
    /// @param json: A simple [Key:Value,] Json string.
    /// @return A C# Hashtable-like version of the Json string.
    public Hashtable SimpleJsonDeserializer(string json)
    {
        Hashtable h = null;
        
        try
        {
        	var pairs = json.Split(",");
        	foreach(string s in pairs)
        	{
        	    string ss = s.Replace("{", "").Replace("}", "").Replace("\"", "").Trim();
        	    var kv = ss.Split(":");
        	    if(null == h)
        	    {
        	    	h = new Hashtable();
        	    }
        	    
        	    if(2 == kv.Length)
        	    {
        	    	h.Add(kv[0].Trim(), kv[1].Trim());
        	    }
        	    else // throw some error exception
        	    {}
        	}
        }
        catch(Exception e)
        {
        	string method = "SimpleJsonDeserializer";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
        }
        
        return h;
    }
    
    public string GetAccessTokenFromOAuth(Hashtable oauth)
    {
    	string accessToken = null;
    	
    	try
    	{
    		foreach(DictionaryEntry p in oauth)
    		{
    			if((new Regex(@"access|token", RegexOptions.IgnoreCase)).IsMatch(p.Key.ToString()))
    			{
    				accessToken = oauth[p.Key].ToString();
    				break;
    			}
    		}
    	}
    	catch(Exception e)
    	{
    		string method = "GetAccessTokenFromOAuth";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return accessToken;
    }
    
    /// @brief Gets the Mpesa OAuth 2.0 needed to access Mpesa APIs
    /// @param key: The Mpesa consumer key.
    /// @param secret: Mpesa consumer secret.
    /// @return The Json OAuth converted into a C# Hashtable
    public Hashtable MpesaOAuth(string key=null, string secret=null)
    {
    	Hashtable oauth_h = null;
    	
    	try
    	{
    		var oauth_t = HttpsMethodHandler(null, "GET", null, null, key, secret);
    		
    		if(null != oauth_t)
    		{
    			oauth_t.Wait();
    			string oauth_s = oauth_t.Result;
    			
    			Console.WriteLine("\r\nAccessToken String: " + oauth_s);
    			
    			oauth_h = SimpleJsonDeserializer(oauth_s);
    		}
    	}
    	catch(Exception e)
    	{
    		string method = "MpesaOAuth";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return oauth_h;
    }
    
    /// @brief Fetches data from online using HTTPS get with basic authentication based on key and secret if any.
    /// @param url: the HTTP or HTTPS url identifying the resource to fetch
    /// @param body: A UTF-8 string of data.
    /// @param headers: Custom HTTP headers to send with the request.
    /// @param key: Some key needed to make a basic auth.
    /// @param secret: Some secret or password used by basic auth module.
    /// @return HTTP or HTTPS resource. If url is null, then it fetches the Mpesa OAuth 2.0
    public Task<string> HttpsMethodHandler(
    	string url, 
    	string http_method="GET",
    	string body=null, 
    	Hashtable headers=null, 
    	string key=null, 
    	string secret=null
    ){
    	
    	NetworkCredential cred = null;
    	Task<string> resource = null;
    	
    	try
    	{
    		
    		if(null == url)
    		{
    			url = OAuthApiResource;
    		}
    		
    		// headers
    		Hashtable hders = new Hashtable();
    		hders.Add("Accept-Encoding", "gzip");
    		hders.Add("Accept-Language", "en-US");
    		
    		// Credentials and Authorization
    		if(!String.IsNullOrEmpty(MyKey) && !String.IsNullOrEmpty(MySecret))
    		{    			
    			key = MyKey;
    			secret = MySecret;
    			
    			cred = new NetworkCredential(key, secret);
    		}
    		
    		if(null == headers)
    		{
    			headers = hders;
    		}
    		else
    		{
    			foreach(DictionaryEntry pair in hders)
    			{
    				headers.Add(pair.Key, pair.Value);
    			}
    		}
    	    
    	    // Request
    		HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
    		request.Accept = "*/*";
    		request.Method = http_method;
    		request.PreAuthenticate = true;
    		
    		if(null != body)
    		{
    			if(headers.ContainsKey("Content-Type"))
    			{
    				request.ContentType = headers["Content-Type"].ToString();
    			}
    			else
    			{
    				request.ContentType = "application/json";
    			}
    			
    			if(headers.ContainsKey("Content-Length"))
    			{
    				request.ContentLength = Int32.Parse(headers["Content-Length"].ToString());
    			}
    			else
    			{
    				byte[] bytes = bytes = System.Text.Encoding.UTF8.GetBytes(body);
    				request.ContentLength = bytes.Length;
    			}
    		}
    		
    		foreach(DictionaryEntry pair in headers)
    		{
    			request.Headers.Add(pair.Key.ToString(), pair.Value.ToString());
    		}
    		
    		if(null != cred)
    		{
    			request.Credentials = cred;
    		}
    		
    		var keys = request.Headers.Keys;
    		foreach(string k in keys)
    		{
    			if((new Regex(@"\bauthorization\b", RegexOptions.IgnoreCase)).IsMatch(k.ToString()))
    			{
    				Console.WriteLine("\r\nAuthorization: " + request.Headers[k]);
    				break;
    			}
    		}
    	    
    	    // Response
    		HttpWebResponse response = (HttpWebResponse)request.GetResponse();
    		Stream stream = response.GetResponseStream();
    		System.Text.Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
			StreamReader readStream = new StreamReader(stream, encode);
    	
    		resource = readStream.ReadToEndAsync();
    	
    		response.Close();
    		readStream.Close();
    	}
    	catch(Exception e)
    	{
    		string method = "HttpsMethodHandler";
    		string errMessage = "LemaMpesa" + "::" + method + "(): " + e.Message;
    		Console.WriteLine(errMessage + "\r\n");
    	}
    	
    	return resource;
    }
}

}
