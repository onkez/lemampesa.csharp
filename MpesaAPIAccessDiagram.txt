
Mpesa API Access Logic Diagram
_______________________________


 _____________________________________________________
|                                                     |
| Https Request with Https Basic Authentication token | Authenticated Request: https://developer.safaricom.co.ke/docs?ruby#authentication
|_____________________________________________________|
                  |                       |
                  |                       |
       ___________|____________           |
      |                        |          |
      | OAuth 2.0 Access token |----------| Mpesa API invocation: https://developer.safaricom.co.ke/docs?ruby#http-header-parameters
      |________________________|          |
                                          |                         
                                          |                        
                           _______________|__________
                          |                          |
                          | Mpesa API Infrastructure | Transaction: https://developer.safaricom.co.ke/docs?ruby#security-credentials
                          |__________________________|
